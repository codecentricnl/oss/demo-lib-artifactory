package nl.codecentric.demo;

import org.junit.Test;

import static org.assertj.core.api.Java6Assertions.assertThat;

public class DemoLibTest
{
    private DemoLib demoLib = new DemoLib();

    @Test
    public void testDep()
    {
        assertThat(demoLib.one()).isEqualTo(1);
    }
}
